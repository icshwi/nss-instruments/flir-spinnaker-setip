//=============================================================================
// Copyright © 2017 FLIR Integrated Imaging Solutions, Inc. All Rights Reserved.
//
// This software is the confidential and proprietary information of FLIR
// Integrated Imaging Solutions, Inc. ("Confidential Information"). You
// shall not disclose such Confidential Information and shall use it only in
// accordance with the terms of the license agreement you entered into
// with FLIR Integrated Imaging Solutions, Inc. (FLIR).
//
// FLIR MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF THE
// SOFTWARE, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE, OR NON-INFRINGEMENT. FLIR SHALL NOT BE LIABLE FOR ANY DAMAGES
// SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR DISTRIBUTING
// THIS SOFTWARE OR ITS DERIVATIVES.
//=============================================================================

/**
*	@example Acquisition.cpp
*
*	@brief Acquisition.cpp shows how to acquire images. It relies on
*	information provided in the Enumeration example. Also, check out the
*	ExceptionHandling and NodeMapInfo examples if you haven't already.
*	ExceptionHandling shows the handling of standard and Spinnaker exceptions
*	while NodeMapInfo explores retrieving information from various node types.
*
*	This example touches on the preparation and cleanup of a camera just before
*	and just after the acquisition of images. Image retrieval and conversion,
*	grabbing image data, and saving images are all covered as well.
*
*	Once comfortable with Acquisition, we suggest checking out
*	AcquisitionMultipleCamera, NodeMapCallback, or SaveToAvi.
*	AcquisitionMultipleCamera demonstrates simultaneously acquiring images from
*	a number of cameras, NodeMapCallback serves as a good introduction to
*	programming with callbacks and events, and SaveToAvi exhibits video creation.
*/

#include "Spinnaker.h"
#include "SpinGenApi/SpinnakerGenApi.h"
#include <iostream>
#include <sstream> 

using namespace Spinnaker;
using namespace Spinnaker::GenApi;
using namespace Spinnaker::GenICam;
using namespace std;

// This function prints the device information of the camera from the transport
// layer; please see NodeMapInfo example for more in-depth comments on printing
// device information from the nodemap.
int PrintDeviceInfo(INodeMap & nodeMap)
{
	int result = 0;

	cout << endl << "*** DEVICE INFORMATION ***" << endl << endl;

	try
	{
		FeatureList_t features;
		CCategoryPtr category = nodeMap.GetNode("DeviceInformation");
		if (IsAvailable(category) && IsReadable(category))
		{
			category->GetFeatures(features);

			FeatureList_t::const_iterator it;
			for (it = features.begin(); it != features.end(); ++it)
			{
				CNodePtr pfeatureNode = *it;
				cout << pfeatureNode->GetName() << " : ";
				CValuePtr pValue = (CValuePtr)pfeatureNode;
				cout << (IsReadable(pValue) ? pValue->ToString() : "Node not readable");
				cout << endl;
			}
		}
		else
		{
			cout << "Device control information not available." << endl;
		}


	}
	catch (Spinnaker::Exception &e)
	{
		cout << "Error: " << e.what() << endl;
		result = -1;
	}

	return result;
}


// Example entry point; please see Enumeration example for more in-depth 
// comments on preparing and cleaning up the system.
int main(int argc, char** argv)
{

  char ip[80];	

  if( argc == 2 ) {
      strcpy(ip, argv[1]);	  
   }
   else {
      cout << "One argument (ip address) is expected, e.g. 192.168.1.1" << endl;
      return 0;
   }

   std::stringstream s(ip);
   int a,b,c,d; 
   char ch; 
   s >> a >> ch >> b >> ch >> c >> ch >> d;
   unsigned int ip_hex = (a<<24) + (b<<16) + (c<<8) + d;
   //std::cout << a << "  " << b << "  " << c << "  "<< d << endl;;
   //std::cout << hex << a << "  " << hex << b << "  " << hex << c << "  "<< hex << d << endl;;
   cout << "new persisten IP will be set to: "<< hex << ip_hex << endl;


	int result = 0;

	// Print application build information
	cout << "Application build date: " << __DATE__ << " " << __TIME__ << endl << endl;

	// Retrieve singleton reference to system object
	SystemPtr system = System::GetInstance();

	// Force IP on all GigE interfaces
	InterfaceList interfaceList = system->GetInterfaces();
	int numInterface = interfaceList.GetSize();
	for (int i = 0; i < numInterface; ++i)
	{
		InterfacePtr pInterface = interfaceList.GetByIndex(i);
		TransportLayerInterface &tLInterface = pInterface->TLInterface;
		if (!tLInterface.InterfaceType.ToString().compare("GEV"))
		{

			cout << "Forcing IP on interface with IP " << ((tLInterface.GevInterfaceIPAddress.GetValue() & 0xFF000000) >> 24)
				<< "." << ((tLInterface.GevInterfaceIPAddress.GetValue() & 0xFF0000) >> 16)
				<< "." << ((tLInterface.GevInterfaceIPAddress.GetValue() & 0xFF00) >> 8)
				<< "." << (tLInterface.GevInterfaceIPAddress.GetValue() & 0xFF) << endl;

			tLInterface.AutoForceIP.Execute();
			//wait for force IP to finish
			//Sleep(3000);

			CameraList camList = pInterface->GetCameras();
			if (camList.GetSize())
			{
				for (int i = 0; i < camList.GetSize(); ++i)
				{
					CameraPtr pCam = NULL;
					pCam = camList.GetByIndex(i);
					try
					{
						// Retrieve TL device nodemap and print device information
						INodeMap & nodeMapTLDevice = pCam->GetTLDeviceNodeMap();

						result = PrintDeviceInfo(nodeMapTLDevice);

						// Initialize camera
						pCam->Init();

						// Retrieve GenICam nodemap
						INodeMap & nodeMap = pCam->GetNodeMap();


						// set persistent IP Address and subnet mask
						cout << endl << endl;

						CBooleanPtr ptrPersistentConfig = nodeMap.GetNode("GevCurrentIPConfigurationPersistentIP");
						if (!IsAvailable(ptrPersistentConfig) || !IsWritable(ptrPersistentConfig))
						{
							cout << "Cannot set/get GevCurrentIPConfigurationPersistentIP ...error node retrieval " << endl;
							return -1;
						}
						ptrPersistentConfig->SetValue(true);

						CIntegerPtr ptrPersistentIPAddress = nodeMap.GetNode("GevPersistentIPAddress");
						if (!IsAvailable(ptrPersistentIPAddress) || !IsWritable(ptrPersistentIPAddress))
						{
							cout << "Cannot set/get GevPersistentIPAddress ...error node retrieval " << endl;
							return -1;
						}
						CIntegerPtr ptrPersistentSubnet = nodeMap.GetNode("GevPersistentSubnetMask");
						if (!IsAvailable(ptrPersistentSubnet) || !IsWritable(ptrPersistentSubnet))
						{
							cout << "Cannot set/get GevPersistentSubnetMask ...error node retrieval " << endl;
							return -1;
						}
						CIntegerPtr ptrPersistentGateway = nodeMap.GetNode("GevPersistentDefaultGateway");
						if (!IsAvailable(ptrPersistentGateway) || !IsWritable(ptrPersistentGateway))
						{
							cout << "Cannot set/get GevPersistentDefaultGateway ...error node retrieval " << endl;
							return -1;
						}


						CIntegerPtr ptrCurrentIPAddress = nodeMap.GetNode("GevCurrentIPAddress");
						if (!IsAvailable(ptrCurrentIPAddress))
						{
							cout << "Cannot set/get GevCurrentIPAddress ...error node retrieval " << endl;
							return -1;
						}
						CIntegerPtr ptrCurrentSubnet = nodeMap.GetNode("GevCurrentSubnetMask");
						if (!IsAvailable(ptrCurrentSubnet))
						{
							cout << "Cannot set/get GevCurrentSubnetMask ...error node retrieval " << endl;
							return -1;
						}
						CIntegerPtr ptrCurrentGateway = nodeMap.GetNode("GevCurrentDefaultGateway");
						if (!IsAvailable(ptrCurrentGateway))
						{
							cout << "Cannot set/get GevCurrentDefaultGateway ...error node retrieval " << endl;
							return -1;
						}
						


						cout << "Persistent IP : " << ptrPersistentIPAddress->GetValue() << endl;
						cout << "Persistent Subnet Mask : " << ptrPersistentSubnet->GetValue() << endl;
						cout << "Persistent Default Gateway : " << ptrPersistentGateway->GetValue() << endl;
						cout << "Current IP : " << ptrCurrentIPAddress->GetValue() << endl;
						cout << "Current Subnet Mask : " << ptrCurrentSubnet->GetValue() << endl;
						cout << "Current Default Gateway : " << ptrCurrentGateway->GetValue() << endl;
						cout << "Interface IP :" << tLInterface.GevInterfaceIPAddress.GetValue() << endl;


						ptrPersistentIPAddress->SetValue(ip_hex);
						//ptrPersistentIPAddress->SetValue(ptrCurrentIPAddress->GetValue());
						ptrPersistentSubnet->SetValue(ptrCurrentSubnet->GetValue());
						ptrPersistentGateway->SetValue(tLInterface.GevInterfaceIPAddress.GetValue());
						

						cout << "New Persistent IP : " << ptrPersistentIPAddress->GetValue() << endl;
						cout << "New Persistent Subnet Mask : " << ptrPersistentSubnet->GetValue() << endl;
						cout << "New Persistent Default Gateway : " << ptrPersistentGateway->GetValue() << endl;


						cout << endl;

						//ResetDevice

						CCommandPtr ptrResetDevice = nodeMap.GetNode("DeviceReset");
						if (!IsAvailable(ptrResetDevice) && !IsWritable(ptrResetDevice))
						{
							cout << "Cannot set/get DeviceReset ...error node retrieval " << endl;
							return -1;
						}
						ptrResetDevice->Execute();

					}
					catch (Spinnaker::Exception &e)
					{
						cout << "Error: " << e.what() << endl;
						result = -1;
					}
				}
			}
			camList.Clear();
		}

	}
	interfaceList.Clear();

	// Release system
	system->ReleaseInstance();

	cout << endl << "Done! Press Enter to exit..." << endl;
	getchar();

	return result;
}
